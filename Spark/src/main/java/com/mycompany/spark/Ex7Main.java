/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark;

import java.util.Collections;
import java.util.Map;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 *
 * @author jasimioni
 */
public class Ex7Main {
    public static void main(String args[]) {
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("pratica");
        JavaSparkContext sc = new JavaSparkContext(conf);
        
        JavaRDD<String> arquivo = sc.textFile("/home/jasimioni/Frameworks/spark/ocorrencias_criminais.csv");
        
        // 7.	Mês com a maior ocorrência de crimes do tipo “DECEPTIVE PRACTICE”
       
        JavaRDD<String> deceptiveRDD = arquivo.filter( s -> {
            String[] campos = s.split(";");
            // tipo = campos[4]
            return campos[4].equalsIgnoreCase("DECEPTIVE PRACTICE");
        });        
        
        JavaRDD<String> deceptivePorMesRDD = deceptiveRDD.map( s-> {
            String[] campos = s.split(";");
            return campos[1];           
        });
        
        Map<String, Long> deceptivePorMes = deceptivePorMesRDD.countByValue(); 
        
        Long maximoDeceptivePorMes = Collections.max(deceptivePorMes.values());
	
        System.out.println("7. Mês com a maior ocorrência de crimes do tipo 'DECEPTIVE PRACTICE'");            
        for ( String key : deceptivePorMes.keySet() ) {
            // Se o número de crimes é igual ao máximo, imprime a chave (pode ter mais de um)
            if (deceptivePorMes.get(key) == maximoDeceptivePorMes) {
                 System.out.println("No mes " + key + " tivemos a maior quantidade de crimes DECEPTIVE PRACTICE (" + maximoDeceptivePorMes + ")");
            }
        }             
    }    
}
