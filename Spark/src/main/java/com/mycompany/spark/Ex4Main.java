/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark;

import java.util.Collections;
import java.util.Map;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 *
 * @author jasimioni
 */
public class Ex4Main {
    public static void main(String args[]) {
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("pratica");
        JavaSparkContext sc = new JavaSparkContext(conf);
        
        JavaRDD<String> arquivo = sc.textFile("/home/jasimioni/Frameworks/spark/ocorrencias_criminais.csv");
        
        // 4.	Mês com maior ocorrência de crimes;
        JavaRDD<String> mesRDD = arquivo.map( s -> {
            String[] campos = s.split(";");
            return campos[1];          
        });
        
        Map<String, Long> crimesPorMes = mesRDD.countByValue();
        
        // Pega o maior número de crimes
        Long maximoCrimes = Collections.max(crimesPorMes.values());
              
        System.out.println("4. Mês com maior ocorrência de crimes:");
        for ( String key : crimesPorMes.keySet() ) {
            // Se o número de crimes é igual ao máximo, imprime a chave (pode ter mais de um)
            if (crimesPorMes.get(key) == maximoCrimes) {
                System.out.println("No mes " + key + " tivemos o maior número de crimes (" + maximoCrimes + ")");
            }
        }
    }    
}
