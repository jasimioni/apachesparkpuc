/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark;

import java.util.Collections;
import java.util.Map;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 *
 * @author jasimioni
 */
public class Ex1Main {
    public static void main(String args[]) {
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("pratica");
        JavaSparkContext sc = new JavaSparkContext(conf);
        
        JavaRDD<String> arquivo = sc.textFile("/home/jasimioni/Frameworks/spark/ocorrencias_criminais.csv");
        
        
        // 1.	Quantidade de crimes por ano      
        JavaRDD<String> anoRDD = arquivo.map( s -> {
            String[] campos = s.split(";");
            return campos[2];          
        });
        
        Map<String, Long> crimesPorAno = anoRDD.countByValue();
        System.out.println("1. Quantidade de crimes por ano:");
        System.out.println(crimesPorAno);            
    }    
}
