/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark;

import java.util.Collections;
import java.util.Map;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 *
 * @author jasimioni
 */
public class Ex5Main {
    public static void main(String args[]) {
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("pratica");
        JavaSparkContext sc = new JavaSparkContext(conf);
        
        JavaRDD<String> arquivo = sc.textFile("/home/jasimioni/Frameworks/spark/ocorrencias_criminais.csv");
	
        // 5.	Mês com a maior média de ocorrência de crimes;
        /*
            Caro professor, a informação aqui está solicitada de forma incompleta.
            Quando você diz média, estou inferindo que é a soma de todos os crimes do mês
            dividido pelos anos. Por exemplo, em abril, a média de crimes é 30, porque no ano
            2011 foi 35, 2012 foi 33, 2013 foi 17, 2014 foi 25 (por exemplo).
        
            Nesse cenário o mês com a maior média é o mesmo mês da maior ocorrência e só vou
            dividir pelos anos da amostra (porque se em abril de 2015, por exemplo, não houve
            nenhum crime, não vai existir uma linha com abril de 2015 mas esse ano existiu  
            e nesse ano devemos considerar como 0 o número de crimes.
        */
        
        JavaRDD<String> anoRDD = arquivo.map( s -> {
            String[] campos = s.split(";");
            return campos[2];          
        });
        
        Map<String, Long> crimesPorAno = anoRDD.countByValue(); // Usado apenas para isolar os anos existentes
        
        JavaRDD<String> mesRDD = arquivo.map( s -> {
            String[] campos = s.split(";");
            return campos[1];          
        });
        
        Map<String, Long> crimesPorMes = mesRDD.countByValue();
        
        // Pega o maior número de crimes
        Long maximoCrimes = Collections.max(crimesPorMes.values());
        int totalAnos = crimesPorAno.keySet().size();
        float maximoMediaCrimes = maximoCrimes / totalAnos;
        
        System.out.println("5. Mês com a maior média de ocorrência de crimes:");
        for ( String key : crimesPorMes.keySet() ) {
            // Se o número de crimes é igual ao máximo, imprime a chave (pode ter mais de um)
            if (crimesPorMes.get(key) == maximoCrimes) {
                System.out.println("No mes " + key + " tivemos a maior média de crimes (" + maximoMediaCrimes + " por mês em média)");
            }
        }
    }    
}
