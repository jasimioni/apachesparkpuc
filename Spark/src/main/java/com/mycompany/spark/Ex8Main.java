/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark;

import java.util.Collections;
import java.util.Map;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 *
 * @author jasimioni
 */
public class Ex8Main {
    public static void main(String args[]) {
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("pratica");
        JavaSparkContext sc = new JavaSparkContext(conf);
        
        JavaRDD<String> arquivo = sc.textFile("/home/jasimioni/Frameworks/spark/ocorrencias_criminais.csv");
        
        // 8.	Dia do ano com a maior ocorrência de crimes;
        JavaRDD<String> diaMesRDD = arquivo.map( s-> {
            String[] campos = s.split(";");
            return campos[0] + ";" + campos[1];           
        });       
        
        Map<String, Long> crimesPorDiaMes = diaMesRDD.countByValue(); 
        
        Long maximoCrimesPorDiaMes = Collections.max(crimesPorDiaMes.values());
        
        System.out.println("8. Dia do ano com a maior ocorrência de crimes:");
        for ( String key : crimesPorDiaMes.keySet() ) {
            if (crimesPorDiaMes.get(key) == maximoCrimesPorDiaMes)  {
                String[] data = key.split(";");
                System.out.println("No dia " + data[0] + " do mês " + data[1] + " temos a maior ocorrência de crimes: " + maximoCrimesPorDiaMes);
            }
        }
     
        
    }    
}
