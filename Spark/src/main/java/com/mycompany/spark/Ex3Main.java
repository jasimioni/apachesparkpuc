/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark;

import java.util.Collections;
import java.util.Map;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 *
 * @author jasimioni
 */
public class Ex3Main {
    public static void main(String args[]) {
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("pratica");
        JavaSparkContext sc = new JavaSparkContext(conf);
        
        JavaRDD<String> arquivo = sc.textFile("/home/jasimioni/Frameworks/spark/ocorrencias_criminais.csv");
        
        // 3.	Quantidade de crimes por ano, que sejam do tipo NARCOTICS, e tenham ocorrido em dias pares; 

        JavaRDD<String> narcoticsRDD = arquivo.filter( s -> {
            String[] campos = s.split(";");
            return campos[4].equalsIgnoreCase("NARCOTICS");
        });
        
        JavaRDD<String> narcoticsDiaParRDD = narcoticsRDD.filter( s-> {
            String[] campos = s.split(";");
            return Integer.parseInt(campos[0]) % 2 == 0; // TRUE se for par
        });
        
        JavaRDD<String> narcoticsDiaParAnoRDD = narcoticsDiaParRDD.map( s -> {
            String[] campos = s.split(";");
            return campos[2];          
        });          
        
        Map<String, Long> narcoticsDiaParPorAno = narcoticsDiaParAnoRDD.countByValue();
        System.out.println("3. Quantidade de crimes por ano, que sejam do tipo NARCOTICS, e tenham ocorrido em dias pares:");
        System.out.println(narcoticsDiaParPorAno);
    }    
}
