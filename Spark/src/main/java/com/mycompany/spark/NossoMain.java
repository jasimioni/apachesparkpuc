/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark;

import java.util.Collections;
import java.util.Map;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 *
 * @author jasimioni
 */
public class NossoMain {
    public static void main(String args[]) {
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("pratica");
        JavaSparkContext sc = new JavaSparkContext(conf);
        
        JavaRDD<String> arquivo = sc.textFile("/home/jasimioni/Frameworks/spark/ocorrencias_criminais_sample.csv");
        
        
        // 1.	Quantidade de crimes por ano      
        JavaRDD<String> anoRDD = arquivo.map( s -> {
            String[] campos = s.split(";");
            return campos[2];          
        });
        
        Map<String, Long> crimesPorAno = anoRDD.countByValue();
        System.out.println("1. Quantidade de crimes por ano:");
        System.out.println(crimesPorAno);            
        
        
        // 2.	Quantidade de crimes por ano que sejam do tipo NARCOTICS
        JavaRDD<String> narcoticsRDD = arquivo.filter( s -> {
            String[] campos = s.split(";");
            // tipo = campos[4]
            return campos[4].equalsIgnoreCase("NARCOTICS");
        });
        
        JavaRDD<String> narcoticsAnoRDD = narcoticsRDD.map( s -> {
            String[] campos = s.split(";");
            return campos[2];          
        });        
        
        Map<String, Long> narcoticsPorAno = narcoticsAnoRDD.countByValue();
        System.out.println("2. Quantidade de crimes por ano que sejam do tipo NARCOTICS:");
        System.out.println(narcoticsPorAno);
               
        
        // 3.	Quantidade de crimes por ano, que sejam do tipo NARCOTICS, e tenham ocorrido em dias pares; 
        JavaRDD<String> narcoticsDiaParRDD = narcoticsRDD.filter( s-> {
            String[] campos = s.split(";");
            return Integer.parseInt(campos[0]) % 2 == 0; // TRUE se for par
        });
        
        JavaRDD<String> narcoticsDiaParAnoRDD = narcoticsDiaParRDD.map( s -> {
            String[] campos = s.split(";");
            return campos[2];          
        });          
        
        Map<String, Long> narcoticsDiaParPorAno = narcoticsDiaParAnoRDD.countByValue();
        System.out.println("3. Quantidade de crimes por ano, que sejam do tipo NARCOTICS, e tenham ocorrido em dias pares:");
        System.out.println(narcoticsDiaParPorAno);
        
        
        // 4.	Mês com maior ocorrência de crimes;
        JavaRDD<String> mesRDD = arquivo.map( s -> {
            String[] campos = s.split(";");
            return campos[1];          
        });
        
        Map<String, Long> crimesPorMes = mesRDD.countByValue();
        
        // Pega o maior número de crimes
        Long maximoCrimes = Collections.max(crimesPorMes.values());
              
        System.out.println("4. Mês com maior ocorrência de crimes:");
        for ( String key : crimesPorMes.keySet() ) {
            // Se o número de crimes é igual ao máximo, imprime a chave (pode ter mais de um)
            if (crimesPorMes.get(key) == maximoCrimes) {
                System.out.println("No mes " + key + " tivemos o maior número de crimes (" + maximoCrimes + ")");
            }
        }
        
        // 5.	Mês com a maior média de ocorrência de crimes;
        /*
            Caro professor, a informação aqui está solicitada de forma incompleta.
            Quando você diz média, estou inferindo que é a soma de todos os crimes do mês
            dividido pelos anos. Por exemplo, em abril, a média de crimes é 30, porque no ano
            2011 foi 35, 2012 foi 33, 2013 foi 17, 2014 foi 25 (por exemplo).
        
            Nesse cenário o mês com a maior média é o mesmo mês da maior ocorrência e só vou
            dividir pelos anos da amostra (porque se em abril de 2015, por exemplo, não houve
            nenhum crime, não vai existir uma linha com abril de 2015 mas esse ano existiu  
            e nesse ano devemos considerar como 0 o número de crimes.
        */
        
        int totalAnos = crimesPorAno.keySet().size();
        float maximoMediaCrimes = maximoCrimes / totalAnos;
        
        System.out.println("5. Mês com a maior média de ocorrência de crimes:");
        for ( String key : crimesPorMes.keySet() ) {
            // Se o número de crimes é igual ao máximo, imprime a chave (pode ter mais de um)
            if (crimesPorMes.get(key) == maximoCrimes) {
                System.out.println("No mes " + key + " tivemos a maior média de crimes (" + maximoMediaCrimes + " por mês em média)");
            }
        }
      
        // 6.	Mês por ano com a maior ocorrência de crimes;
        // Já temos os anos no RDD crimesPorAno
        
        for ( String ano : crimesPorAno.keySet() ) {
            // Filtrar apenas do ano em questão
            JavaRDD<String> crimesAnoAtual = arquivo.filter( s-> {
                String[] campos = s.split(";");
                return campos[2].equals(ano);
            });        
            
            JavaRDD<String> mesAnoAtualRDD = crimesAnoAtual.map( s -> {
                String[] campos = s.split(";");
                return campos[1];          
            });
        
            Map<String, Long> crimesPorMesAnoAtual = mesAnoAtualRDD.countByValue();     
            
            Long maximoAnoAtualCrimes = Collections.max(crimesPorMesAnoAtual.values());
            
            for ( String key : crimesPorMesAnoAtual.keySet() ) {
                // Se o número de crimes é igual ao máximo, imprime a chave (pode ter mais de um)
                if (crimesPorMesAnoAtual.get(key) == maximoAnoAtualCrimes) {
                    System.out.println("Em " + ano + " a maior quantidade de crimes ocorreu no mes " + key + " (" + maximoAnoAtualCrimes + ")");
                }
            }                
        }

        
        // 7.	Mês com a maior ocorrência de crimes do tipo “DECEPTIVE PRACTICE”
        
        JavaRDD<String> deceptiveRDD = arquivo.filter( s -> {
            String[] campos = s.split(";");
            // tipo = campos[4]
            return campos[4].equalsIgnoreCase("DECEPTIVE PRACTICE");
        });        
        
        JavaRDD<String> deceptivePorMesRDD = deceptiveRDD.map( s-> {
            String[] campos = s.split(";");
            return campos[1];           
        });
        
        Map<String, Long> deceptivePorMes = deceptivePorMesRDD.countByValue(); 
        
        Long maximoDeceptivePorMes = Collections.max(deceptivePorMes.values());
            
        for ( String key : deceptivePorMes.keySet() ) {
            // Se o número de crimes é igual ao máximo, imprime a chave (pode ter mais de um)
            if (deceptivePorMes.get(key) == maximoDeceptivePorMes) {
                 System.out.println("No mes " + key + " tivemos a maior quantidade de crimes DECEPTIVE PRACTICE (" + maximoDeceptivePorMes + ")");
            }
        }             
        
        // 8.	Dia do ano com a maior ocorrência de crimes;
        JavaRDD<String> diaMesRDD = arquivo.map( s-> {
            String[] campos = s.split(";");
            return campos[0] + ";" + campos[1];           
        });       
        
        Map<String, Long> crimesPorDiaMes = diaMesRDD.countByValue(); 
        
        Long maximoCrimesPorDiaMes = Collections.max(crimesPorDiaMes.values());
        
        for ( String key : crimesPorDiaMes.keySet() ) {
            if (crimesPorDiaMes.get(key) == maximoCrimesPorDiaMes)  {
                String[] data = key.split(";");
                System.out.println("No dia " + data[0] + " do mês " + data[1] + " temos a maior ocorrência de crimes: " + maximoCrimesPorDiaMes);
            }
        }
     
        
    }    
}
