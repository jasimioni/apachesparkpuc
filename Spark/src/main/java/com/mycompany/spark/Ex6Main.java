/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark;

import java.util.Collections;
import java.util.Map;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 *
 * @author jasimioni
 */
public class Ex6Main {
    public static void main(String args[]) {
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("pratica");
        JavaSparkContext sc = new JavaSparkContext(conf);
        
        JavaRDD<String> arquivo = sc.textFile("/home/jasimioni/Frameworks/spark/ocorrencias_criminais.csv");
        
        // 6.	Mês por ano com a maior ocorrência de crimes;
        JavaRDD<String> anoRDD = arquivo.map( s -> {
            String[] campos = s.split(";");
            return campos[2];          
        });
        
        Map<String, Long> crimesPorAno = anoRDD.countByValue(); // Somente para pegar todos os anos
        
        System.out.println("6. Mês por ano com a maior ocorrência de crimes:");
        for ( String ano : crimesPorAno.keySet() ) {
            // Filtrar apenas do ano em questão
            JavaRDD<String> crimesAnoAtual = arquivo.filter( s-> {
                String[] campos = s.split(";");
                return campos[2].equals(ano);
            });        
            
            JavaRDD<String> mesAnoAtualRDD = crimesAnoAtual.map( s -> {
                String[] campos = s.split(";");
                return campos[1];          
            });
        
            Map<String, Long> crimesPorMesAnoAtual = mesAnoAtualRDD.countByValue();     
            
            Long maximoAnoAtualCrimes = Collections.max(crimesPorMesAnoAtual.values());
            
            for ( String key : crimesPorMesAnoAtual.keySet() ) {
                // Se o número de crimes é igual ao máximo, imprime a chave (pode ter mais de um)
                if (crimesPorMesAnoAtual.get(key) == maximoAnoAtualCrimes) {
                    System.out.println("Em " + ano + " a maior quantidade de crimes ocorreu no mes " + key + " (" + maximoAnoAtualCrimes + ")");
                }
            }                
        }
    }    
}
