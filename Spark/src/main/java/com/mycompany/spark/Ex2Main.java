/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark;

import java.util.Collections;
import java.util.Map;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 *
 * @author jasimioni
 */
public class Ex2Main {
    public static void main(String args[]) {
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("pratica");
        JavaSparkContext sc = new JavaSparkContext(conf);
        
        JavaRDD<String> arquivo = sc.textFile("/home/jasimioni/Frameworks/spark/ocorrencias_criminais.csv");
        
        // 2.	Quantidade de crimes por ano que sejam do tipo NARCOTICS
        JavaRDD<String> narcoticsRDD = arquivo.filter( s -> {
            String[] campos = s.split(";");
            return campos[4].equalsIgnoreCase("NARCOTICS");
        });
        
        JavaRDD<String> narcoticsAnoRDD = narcoticsRDD.map( s -> {
            String[] campos = s.split(";");
            return campos[2];          
        });        
        
        Map<String, Long> narcoticsPorAno = narcoticsAnoRDD.countByValue();
        System.out.println("2. Quantidade de crimes por ano que sejam do tipo NARCOTICS:");
        System.out.println(narcoticsPorAno);
    }    
}
