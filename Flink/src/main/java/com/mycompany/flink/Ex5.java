/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flink;
// https://ci.apache.org/projects/flink/flink-docs-stable/dev/stream/operators/

import java.util.HashMap;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSink;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 *
 * @author jasimioni
 */
public class Ex5 {
    public static void main(String []args) throws Exception {
        StreamExecutionEnvironment see = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStream<String> arquivoBase = see.readTextFile("/home/jasimioni/Frameworks/flink/ocorrencias_criminais.csv");
        
        
        DataStream<Tuple2<String, Integer>> dataStream = arquivoBase
                .map(new Splitter())
                .filter(new FilterFunction<Tuple2<String, Integer>>() {
                    @Override
                    public boolean filter(Tuple2<String, Integer> t) {
                        String[] data = t.f0.split("-");
                        return Integer.parseInt(data[2]) == 1; // Somente dias = 1
                    }
                })
                .keyBy(0)
                .countWindowAll(10000)
                .sum(1);

        dataStream.print();

        see.execute("Window WordCount");
    }

    public static class Splitter implements MapFunction<String, Tuple2<String, Integer>> {
        @Override
        public Tuple2<String, Integer> map(String line) {
            Integer count = 0;
            String[] campos = line.split(";");
            if (campos[4].equalsIgnoreCase("NARCOTICS")) {
                count = 1;
            }
            String dia = String.format("%04d-%02d-%02d", Integer.parseInt(campos[2]), Integer.parseInt(campos[1]), Integer.parseInt(campos[0]));          
            
            return new Tuple2<> (dia, count);
        }
    }  
}
