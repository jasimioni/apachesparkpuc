/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flink;
// https://ci.apache.org/projects/flink/flink-docs-stable/dev/stream/operators/

import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSink;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 *
 * @author jasimioni
 */
public class Ex1 {
    public static void main(String []args) throws Exception {
        StreamExecutionEnvironment see = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStream<String> arquivoBase = see.readTextFile("/home/jasimioni/Frameworks/flink/ocorrencias_criminais.csv");
               
        DataStream<Integer> resultados = arquivoBase.countWindowAll(10000).aggregate(new AggregateFunction<String, Integer, Integer>() {
            @Override
            public Integer createAccumulator() {
                return 0;
            }

            @Override
            public Integer add(String in, Integer acc) {
                String[] campos = in.split(";");
                if (campos[4].equalsIgnoreCase("NARCOTICS")) {
                    return acc + 1;
                }              
                return acc;
            }

            @Override
            public Integer getResult(Integer acc) {
                return acc;
            }

            @Override
            public Integer merge(Integer acc, Integer acc1) {
                return acc + acc1;
            }
        });
        
        resultados.print();
               
        see.execute();            
    }   
}
