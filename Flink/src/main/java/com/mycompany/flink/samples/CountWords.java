package com.mycompany.flink;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

/**
 *
 * @author jasimioni
 */
public class CountWords {
    public static void main(String[] args) throws Exception {
        
        // /home/jasimioni/NetBeansProjects/Flink/src/main/java/com/mycompany/flink/arquivo

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStream<Tuple2<String, Integer>> dataStream = env
                // .socketTextStream("localhost", 9999)
                .readTextFile("/home/jasimioni/NetBeansProjects/Flink/src/main/java/com/mycompany/flink/arquivo")
                .map(new Splitter())
                .keyBy(0)
                .countWindowAll(5)
                .sum(1);

        dataStream.print();

        env.execute("Window WordCount");
    }

    public static class Splitter implements MapFunction<String, Tuple2<String, Integer>> {
        @Override
        public Tuple2<String, Integer> map(String word) {
            return new Tuple2<> (word, 1);
        }
    }    
}
