/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flink;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 *
 * @author jasimioni
 */
public class CrimesByType {
    public static void main(String []args) throws Exception {
        StreamExecutionEnvironment see = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStream<String> arquivoBase = see.readTextFile("/home/jasimioni/Frameworks/flink/ocorrencias_criminais_sample.csv");
               
        SingleOutputStreamOperator<Tuple2<String, Integer>> resultados = arquivoBase.map(new Splitter())
                .keyBy(0)
                // .countWindowAll(10000)
                .sum(1);
        
        resultados.print();
               
        see.execute();            
    }      
    
    public static class Splitter implements MapFunction<String, Tuple2<String, Integer>> {
        @Override
        public Tuple2<String, Integer> map(String line) {
            String[] campos = line.split(";");
            return new Tuple2<> (campos[4].toUpperCase(), 1);
        }
    }        
    
}
